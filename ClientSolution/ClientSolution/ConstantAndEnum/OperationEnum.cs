﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public enum OperationType
    {
        Connect = 1,
        Publish,
        Pull,
        Help,
        Topic,
        Exit
    }
}
