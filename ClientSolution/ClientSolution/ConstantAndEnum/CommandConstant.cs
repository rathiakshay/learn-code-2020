﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class CommandConstant
    {
        public const string PREFIX = "imq";
        public const string HELP = "help";
        public const string CONNECT = "connect";
        public const string TOPIC = "topic";
        public const string PUBLISH = "publish";
        public const string PULL = "pull";
        public const string PUBLISH_OPTION = "-m";
        public const string EXIT = "exit";
        public const string MESSAGE_CONTENT = "\"message content\"";
        public const string TOPIC_NAME = "\"topic name\"";
        public const string PUBLISH_COMMAND = PREFIX + " " + PUBLISH + " " + PUBLISH_OPTION + " " + TOPIC_NAME + " " + MESSAGE_CONTENT;
        public const string PULL_COMMAND = PREFIX + " " + PULL + " " + TOPIC_NAME;
        public const string HELP_COMMAND = PREFIX + " " + HELP;
        public const string EXIT_COMMAND = PREFIX + " " + EXIT;
        public const string CONNECT_COMMAND = PREFIX + " " + CONNECT + " " + TOPIC_NAME;
        public const string TOPIC_COMMAND = PREFIX + " " + TOPIC;
    }
}
