﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class Config
    {
        public static string ipAddress = "127.0.0.1";
        public static int portNumber = 8000;
        public static int bufferSize = 2048;
    }
}
