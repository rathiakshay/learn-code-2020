﻿using ClientSolution.Services;
using Newtonsoft.Json;
using ProtocolLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class ClientController
    {
        string ipAddress;
        int portNumber;
        TcpClient socketForServer = null;
        NetworkStream networkStream = null;
        CommunicationHandler objCommunicationHandler = null;
        CommandHandler objCommandHandler;
        ClientService objClientService;
        const string STARTING_MESSAGE = "*******This is client program who is connected to localhost on port :";
        const string HELP_MESSAGE = "For help type \"imq help\"";
        public ClientController(string ipAddress, int portNumber)
        {
            this.ipAddress = ipAddress;
            this.portNumber = portNumber;
            this.objCommunicationHandler = new CommunicationHandler();
            this.objCommandHandler = new CommandHandler();
        }
        public bool connectToServer()
        {
            socketForServer = new TcpClient(this.ipAddress, this.portNumber);
            networkStream = socketForServer.GetStream();
            objClientService = new ClientService(socketForServer, networkStream);

            try
            {
                Console.WriteLine(STARTING_MESSAGE + socketForServer.Client.LocalEndPoint);
                Console.WriteLine(HELP_MESSAGE);
                List<string> topics = getTopicsFromServer();
                int topicId = 0;
                while (true)
                {
                    try
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("\n>>>");
                        string inputString = Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.White;

                        int operation = objCommandHandler.selectOperation(inputString);
                        if (isConnectedWithTopic(topicId, operation))
                        {
                            switch (operation)
                            {
                                case (int)ClientSolution.OperationType.Topic:
                                    if (topics != null && topics.Count > 1)
                                    {
                                        int index = 0;
                                        foreach (var topic in topics)
                                        {
                                            Console.WriteLine((++index).ToString() + ": " + topic);
                                        }
                                    }
                                    break;
                                case (int)ClientSolution.OperationType.Connect:
                                    topicId = getTopicID(inputString, topics);
                                    connectWithTopic(topicId);
                                    break;
                                case (int)ClientSolution.OperationType.Publish:
                                    string messageContent = objCommandHandler.getMessageContentFromCommand(inputString);
                                    if (isTopicIdCorrect(topicId, inputString, topics))
                                    {
                                        objClientService.publishDataToTopics(topicId, messageContent);
                                    }
                                    break;
                                case (int)ClientSolution.OperationType.Pull:
                                    if (isTopicIdCorrect(topicId, inputString, topics))
                                    {
                                        displayTopicData(topicId);
                                    }
                                    break;
                                case (int)ClientSolution.OperationType.Help:
                                    objCommandHandler.displayAllCommand();
                                    break;
                                case (int)ClientSolution.OperationType.Exit:
                                    objClientService.closeServerConnection();
                                    return true;
                                default:
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public void connectWithTopic(int topicId)
        {
            objClientService.connectWithTopic(topicId);
        }
        public bool isConnectedWithTopic(int topicId, int operationType)
        {
            if (topicId != 0 || (operationType != (int)ClientSolution.OperationType.Publish && operationType != (int)ClientSolution.OperationType.Pull))
            {
                return true;
            }
            throw new TopicNotConnectedException();
        }
        public bool isTopicIdCorrect(int connectedTopicId, string inputCommandString, List<string> topics)
        {
            try
            {
                int topicIdFromInputCommandString = getTopicID(inputCommandString, topics);
                if (topicIdFromInputCommandString == connectedTopicId)
                {
                    return true;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            throw new InvalidTopicConnectionException();
        }
        public List<string> getTopicsFromServer()
        {
            List<string> topics = objClientService.getTopicsFromServer();
            return topics;
        }
        public int getTopicID(string inputString, List<string> topics)
        {
            try
            {
                string topicName = objCommandHandler.getTopicNameFromCommand(inputString);
                if (topicName != null)
                {
                    int index = 1;
                    foreach (var topic in topics)
                    {
                        if (topic.ToLower() == topicName.ToLower())
                        {
                            return index;
                        }
                        index++;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            throw new TopicDoesNotExistException();
        }
        public void displayTopicData(int topicId)
        {
            foreach (var message in getDataFromTopic(Convert.ToInt32(topicId)))
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine("->" + message);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
        public List<string> getDataFromTopic(int topicId)
        {
            List<string> topicContent = objClientService.getDataFromTopic(topicId);
            return topicContent;
        }
    }
}
