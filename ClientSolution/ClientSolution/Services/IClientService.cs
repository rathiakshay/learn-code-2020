﻿using ProtocolLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution.Services
{
    interface IClientService
    {
        List<string> getTopicsFromServer();
        void publishDataToTopics(int topicId, string message);
        List<string> getDataFromTopic(int topicId);
        void connectWithTopic(int topicId);
        bool closeServerConnection();
    }
}
