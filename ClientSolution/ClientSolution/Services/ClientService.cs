﻿using ProtocolLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution.Services
{
    public class ClientService: IClientService
    {
        CommunicationHandler objCommunicationHandler;
        TcpClient socketForServer;
        NetworkStream networkStream;
        public ClientService(TcpClient socketForServer, NetworkStream networkStream)
        {
            this.socketForServer = socketForServer;
            this.networkStream = networkStream;
            this.objCommunicationHandler = new CommunicationHandler();
        }
        public List<string> getTopicsFromServer()
        {
            Request request = createRequestObjectByRequestType(RequestType.GET_TOPICS, "Publisher");
            objCommunicationHandler.sendRequestToServer(request, networkStream);

            Response response = objCommunicationHandler.getResponseFromServer(networkStream);
            return response.Data;
        }
        public void publishDataToTopics(int topicId, string str)
        {
            List<string> messages = new List<string>();
            messages.Add(str);
            Request request = createRequestObjectByRequestType(RequestType.PUBLISH, "Publisher", messages);
            request.TopicId = topicId;
            objCommunicationHandler.sendRequestToServer(request, networkStream);

            Response response = objCommunicationHandler.getResponseFromServer(networkStream);
            Console.WriteLine("\n>>>" + response.Status);
        }
        private Request createRequestObjectByRequestType(string requestType, string role, [Optional] List<string> data)
        {
            Request request = new Request();
            if (data == null)
            {
                data = new List<string>();
            }
            request.RequestType = requestType;
            request.SourceURI = GetIPAddress();
            request.DestinationURI = Config.ipAddress+":"+Config.portNumber;
            request.Role = role;
            request.Data = data;
            return request;
        }
        private string GetIPAddress()
        {
            return socketForServer.Client.LocalEndPoint.ToString();
        }
        public List<string> getDataFromTopic(int topicId)
        {
            Request request = createRequestObjectByRequestType(RequestType.GET_TOPIC_DATA, "Subscriber");
            request.TopicId = topicId;
            objCommunicationHandler.sendRequestToServer(request, networkStream);

            Response response = objCommunicationHandler.getResponseFromServer(networkStream);
            Console.WriteLine("\n>>>" + response.Status);
            return response.Data;
        }
        public void connectWithTopic(int topicId)
        {
            Request request = createRequestObjectByRequestType(RequestType.CONNECT_TO_TOPIC, "Subscriber");
            request.TopicId = topicId;
            objCommunicationHandler.sendRequestToServer(request, networkStream);

            Response response = objCommunicationHandler.getResponseFromServer(networkStream);
            Console.WriteLine("\n>>>" + response.Status);
        }
        public bool closeServerConnection()
        {
            Request request = createRequestObjectByRequestType(RequestType.EXIT, "Publisher");
            objCommunicationHandler.sendRequestToServer(request, networkStream);
            Response response = objCommunicationHandler.getResponseFromServer(networkStream);
            if (response != null && response.Status.ToLower() == ResponseType.SUCCESSFUL)
            {
                Console.WriteLine("\n\nResponse from server>> " + response.Status.ToLower());
                try
                {
                    objCommunicationHandler.closeConnection(socketForServer.Client, networkStream);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
