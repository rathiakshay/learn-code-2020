﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    class ClientSide
    {
        static void Main(string[] args)
        {
            string ipAddress = Config.ipAddress;
            int portNumber = Config.portNumber;

            ClientController objServerHandler = new ClientController(ipAddress, portNumber);
            try
            {
                if (objServerHandler.connectToServer())
                {
                    Console.WriteLine("Disconnected Succesfully");
                }
                else
                {
                    Console.WriteLine("Not able to connect with server ");
                }
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.Message);
            }
            Console.ReadKey();
        }
    }
}
