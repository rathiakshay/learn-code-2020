﻿using Newtonsoft.Json;
using ProtocolLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class CommunicationHandler
    {
        const string DISCONNECTION_MESSAGE = " has been disconnected";
        public void sendRequestToServer(Request request, NetworkStream networkStream)
        {
            StreamWriter streamWriter = new StreamWriter(networkStream);
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
        }
        public Response getResponseFromServer(NetworkStream networkStream)
        {
            byte[] readBuffer = new byte[2048];
            int numberOfBytesRead = networkStream.Read(readBuffer, 0, readBuffer.Length);
            Response response = JsonConvert.DeserializeObject<Response>(Encoding.ASCII.GetString(readBuffer, 0, numberOfBytesRead).ToString());
            return response;
        }
        public void closeConnection(Socket socketForClient, NetworkStream networkStream)
        {
            Console.WriteLine(socketForClient.RemoteEndPoint + DISCONNECTION_MESSAGE);
            networkStream.Close();
            socketForClient.Close();
        }
    }
}
