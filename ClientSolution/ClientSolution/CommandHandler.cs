﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class CommandHandler
    {
        public int selectOperation(string commandString)
        {
            try
            {
                if (commandString == null || commandString == "")
                {
                    throw new EmptyCommandInputException();
                }
                string[] commandStringArray = commandString.Split(null);

                if (commandStringArray != null && isCommandStringValid(commandStringArray))
                {
                    switch (commandStringArray[1].ToLower())
                    {
                        case CommandConstant.TOPIC:
                            return (int)ClientSolution.OperationType.Topic;
                        case CommandConstant.CONNECT:
                            return (int)ClientSolution.OperationType.Connect;
                        case CommandConstant.PUBLISH:
                            return (int)ClientSolution.OperationType.Publish;
                        case CommandConstant.PULL:
                            return (int)ClientSolution.OperationType.Pull;
                        case CommandConstant.HELP:
                            return (int)ClientSolution.OperationType.Help;
                        case CommandConstant.EXIT:
                            return (int)ClientSolution.OperationType.Exit;
                        default:
                            break;
                    }
                }
                else
                {
                    throw new InvalidCommandException();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            throw new InvalidCommandException();
        }
        private bool isCommandStringValid(string[] commandStringArray)
        {
            if (commandStringArray != null && (commandStringArray[0].ToLower() == CommandConstant.PREFIX))
            {
                if (commandStringArray[1] != null && (commandStringArray[1].ToLower() == CommandConstant.CONNECT ||
                   commandStringArray[1].ToLower() == CommandConstant.TOPIC ||
                   commandStringArray[1].ToLower() == CommandConstant.PUBLISH ||
                   commandStringArray[1].ToLower() == CommandConstant.PULL ||
                   commandStringArray[1].ToLower() == CommandConstant.HELP ||
                   commandStringArray[1].ToLower() == CommandConstant.EXIT
                   ))
                {
                    switch (commandStringArray[1].ToLower())
                    {
                        case CommandConstant.CONNECT:
                            if (commandStringArray[2] != null)
                            {
                                return true;
                            }
                            break;
                        case CommandConstant.PUBLISH:
                            if (commandStringArray[2] != null && commandStringArray[2].ToLower() == CommandConstant.PUBLISH_OPTION && commandStringArray[3] != null)
                            {
                                return true;
                            }
                            break;
                        case CommandConstant.PULL:
                            if (commandStringArray[2] != null)
                            {
                                return true;
                            }
                            break;
                        case CommandConstant.HELP:
                            return true;
                        case CommandConstant.TOPIC:
                            return true;
                        case CommandConstant.EXIT:
                            return true;
                        default:
                            break;
                    }
                }
            }
            return false;
        }
        public string getTopicNameFromCommand(string commandString)
        {
            string topicName = "";
            try
            {
                string[] commandStringArray = commandString.Split(null);
                if (commandStringArray[1].ToLower() == CommandConstant.PUBLISH.ToLower() && !String.IsNullOrEmpty(commandStringArray[3]))
                {
                    topicName = commandStringArray[3];
                }
                else if (!String.IsNullOrEmpty(commandStringArray[2]))
                {
                    topicName = commandStringArray[2];
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            if (!String.IsNullOrEmpty(topicName))
            {
                return topicName;
            }
            throw new NoTopicReferenceException();
        }
        public string getMessageContentFromCommand(string inputString)
        {
            string[] commandStringArray = inputString.Split(null);
            string message = "";
            if (commandStringArray[4] != null)
            {
                for (int index = 4; index < commandStringArray.Length; index++)
                {
                    message += commandStringArray[index] + " ";
                }
            }
            return message;
        }
        public void displayAllCommand()
        {
            Console.WriteLine("\n->" + CommandConstant.TOPIC_COMMAND);
            Console.WriteLine("->" + CommandConstant.CONNECT_COMMAND);
            Console.WriteLine("->" + CommandConstant.PUBLISH_COMMAND);
            Console.WriteLine("->" + CommandConstant.PULL_COMMAND);
            Console.WriteLine("->" + CommandConstant.HELP_COMMAND);
            Console.WriteLine("->" + CommandConstant.EXIT_COMMAND + "\n");
        }
    }
}
