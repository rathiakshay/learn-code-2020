﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSolution
{
    public class InvalidCommandException : Exception
    {
        public InvalidCommandException()
            : base("Command is Invalid, Please enter it again or for help type \"imq help\"")
        {
        }
    }
    public class EmptyCommandInputException : Exception
    {
        public EmptyCommandInputException()
            : base("Command line input empty, Please enter it again or for help type \"imq help\"")
        {
        }
    }
    public class TopicNotConnectedException : Exception
    {
        public TopicNotConnectedException()
            : base("Please connect with any of the topic first, Please enter it again or to connect new topic type \"imq connect \"topic name\"\"")
        {
        }
    }
    public class InvalidTopicConnectionException : Exception
    {
        public InvalidTopicConnectionException()
            : base("Provided topic in command is not connected, Please enter it again or to connect new topic type \"imq connect \"topic name\"\"")
        {
        }
    }
    public class NoTopicReferenceException : Exception
    {
        public NoTopicReferenceException()
            : base("Can not able to fetch topic name from command ,Please enter topic name in command or for help type \"imq help\"")
        {
        }
    }
    public class TopicDoesNotExistException : Exception
    {
        public TopicDoesNotExistException()
            : base("Provided topic does not exist, For available topics type \"imq topic\"")
        {
        }
    }
}
