﻿using ClientSolution;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest.UnitTestForIMQClient
{
    [TestClass]
    public class UnitTestForClientController
    {
        [TestMethod]
        public void TestForIsConnectedWithTopic()
        {
            ClientController objClientController = new ClientController(Config.ipAddress, Config.portNumber);
            int topicId = 1;
            int operationType = 2;
            var actualResult = objClientController.isConnectedWithTopic(topicId, operationType);
            Assert.IsTrue(actualResult);
        }
        [TestMethod]
        public void TestForisTopicIdCorrect()
        {
            ClientController objClientController = new ClientController(Config.ipAddress, Config.portNumber);
            int topicId = 1;
            string command = "imq pull cpp";
            List<string> topics = new List<string>() { "cpp", "algo" };
            var actualResult = objClientController.isTopicIdCorrect(topicId, command, topics);
            Assert.IsTrue(actualResult);
        }

        [TestMethod]
        public void TestForGetTopicID()
        {
            ClientController objClientController = new ClientController(Config.ipAddress, Config.portNumber);
            string command = "imq pull cpp";
            List<string> topics = new List<string>() { "cpp", "algo" };
            var actualResult = objClientController.getTopicID(command, topics);
            var expectedResult = 1;
            Assert.AreEqual(actualResult, expectedResult);
        }
    }
}
