﻿using ClientSolution;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest
{
    [TestClass]
    public class IMQClientUnitTest
    {
        [TestMethod]
        public void TestForIMQTopicCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq topic";
            int actualResult = objCommandHandler.selectOperation(command);
            int expectedResult = (int)OperationType.Topic;
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void TestForIMQConnectCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq connect cpp";
            int actualResult = objCommandHandler.selectOperation(command);
            int expectedResult = (int)OperationType.Connect;
            Assert.AreEqual(actualResult, expectedResult);
        }
        [TestMethod]
        public void TestForIMQPublishCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq publish -m cpp cpp is good";
            int actualResult = objCommandHandler.selectOperation(command);
            int expectedResult = (int)OperationType.Publish;
            Assert.AreEqual(actualResult, expectedResult);
        }
        [TestMethod]
        public void TestForIMQPullCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq pull cpp";
            int actualResult = objCommandHandler.selectOperation(command);
            int expectedResult = (int)OperationType.Pull;
            Assert.AreEqual(actualResult, expectedResult);
        }
        [TestMethod]
        public void TestForIMQExitCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq exit";
            int actualResult = objCommandHandler.selectOperation(command);
            int expectedResult = (int)OperationType.Exit;
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void TestForNotAbleToMakeConnection()
        {
            ClientController objClientController = new ClientController(Config.ipAddress, Config.portNumber);
            try
            {
                objClientController.connectToServer();
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is Exception);
            }
        }
        [TestMethod]
        public void TestForInvalidCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "iqm";
            try
            {
                objCommandHandler.selectOperation(command);
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is InvalidCommandException);
            }
        }
        [TestMethod]
        public void TestForEmptyCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "";
            try
            {
                objCommandHandler.selectOperation(command);
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is EmptyCommandInputException);
            }
        }
        [TestMethod]
        public void TestForGetMessageContentFromCommand()
        {
            CommandHandler objCommandHandler = new CommandHandler();
            string command = "imq publish -m cpp cpp is the best";

            var actualResult = objCommandHandler.getMessageContentFromCommand(command);
            var expectedResult = "cpp is the best ";

            Assert.AreEqual(actualResult, expectedResult);
        }
        
    }
}
