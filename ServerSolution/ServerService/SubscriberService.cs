﻿using IMQModel;
using ServerService.DBQueries;
using ServerService.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService
{
    public class SubscriberService : ISubscriberService
    {
        string connectionString;
        CommonService commonService;
        public SubscriberService()
        {
            connectionString = Config.connectionString;
            commonService = new CommonService();
        }

        public List<Message> getMessageByTopicId(int topicId)
        {
            List<Message> topicData = new List<Message>();
            string queryString = SubscriberServiceQueries.GET_MESSAGE_BY_TOPICID_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(SubscriberServiceQueries.TOPICID_CONSTANT, topicId);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null)
                            {
                                Message objMessage = new Message();
                                objMessage.Content = reader[0].ToString();
                                objMessage.MessageId = Convert.ToInt32(reader[1].ToString());
                                objMessage.TopicId = Convert.ToInt32(reader[2].ToString());
                                topicData.Add(objMessage);
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return topicData;
        }
        public List<int> getMessagesAlreadyPresentForClient(int clientId)
        {
            string queryString = SubscriberServiceQueries.GET_MESSAGE_ALREADY_PRESENT_BY_CLIENTID_QUERY;
            List<int> messages = new List<int>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(SubscriberServiceQueries.CLIENTID_CONSTANT, clientId);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null)
                            {
                                int messageId = Convert.ToInt32(reader[0].ToString());
                                messages.Add(messageId);
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return messages;
        }
        public void updateSubscriberMessageMapTable(int clientId, List<int> newMessage)
        {
            try
            {
                if (newMessage != null)
                {
                    foreach (var messageId in newMessage)
                    {
                        string queryString = SubscriberServiceQueries.UPDATE_SUBSCRIBER_MESSAGE_MAP_TABLE_QUERY;

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue(SubscriberServiceQueries.CLIENTID_CONSTANT, clientId);
                            command.Parameters.AddWithValue(SubscriberServiceQueries.MESSAGEID_CONSTANT, messageId);
                            connection.Open();
                            command.ExecuteReader();
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
