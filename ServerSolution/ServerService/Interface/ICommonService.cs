﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.Interface
{
    interface ICommonService
    {
        bool isClientExist(string clientName);
        bool IsRootTableExist();
        int getClientIdByClientName(string clientName);
        void addClient(string clientName);
        List<Topic> getTopics();
        void updateMessageTable();
        string getClientNameFromClientIp(string clientIp);
        void createNewTopic(string topic);
    }
}
