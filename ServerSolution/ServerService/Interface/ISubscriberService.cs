﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.Interface
{
    interface ISubscriberService
    {
        List<Message> getMessageByTopicId(int topicId);
        List<int> getMessagesAlreadyPresentForClient(int clientId);
        void updateSubscriberMessageMapTable(int clientId, List<int> newMessage);
    }
}
