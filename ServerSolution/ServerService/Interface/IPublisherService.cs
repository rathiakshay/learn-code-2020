﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService
{
    public interface IPublisherService
    {
        Message setData(string message, string publisherIp, int topicId);
        void mapPublisherMessage(int clientId, int messageId);
        void saveDataInMessageTable(string data, int clientId, int topicId);
    }
}
