﻿using IMQModel;
using ServerService.DBQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ServerService
{
    public class PublisherService : IPublisherService
    {
        string connectionString;
        CommonService commonService;
        private const string CLIENT_PREFIX = "Client";
        public PublisherService()
        {
            connectionString = Config.connectionString;
            commonService = new CommonService();
        }

        public Message setData(string message, string publisherIp, int topicId)
        {
            Message objMessage = new Message();
            try
            {
                string[] ipAddressParts = publisherIp.Split(':');
                string clientName = CLIENT_PREFIX + ipAddressParts[ipAddressParts.Length - 1];
                if (!commonService.isClientExist(clientName))
                {
                    commonService.addClient(clientName);
                }
                int clientId = commonService.getClientIdByClientName(clientName);
                saveDataInMessageTable(message, clientId, topicId);
                objMessage = fetchLatestMessage();
                mapPublisherMessage(clientId, objMessage.MessageId);
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return objMessage;
        }

        public void mapPublisherMessage(int clientId, int messageId)
        {
            string queryString = PublisherServiceQueries.MAP_PUBLISHER_TOPIC_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(PublisherServiceQueries.CLIENTID_CONSTANT, clientId);
                    command.Parameters.AddWithValue(PublisherServiceQueries.MessageID_CONSTANT, messageId);
                    connection.Open();
                    command.ExecuteReader();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public void saveDataInMessageTable(string data, int clientId, int topicId)
        {
            string queryString = PublisherServiceQueries.SAVE_DATA_INTO_MESSAGE_TABLE_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(PublisherServiceQueries.DATA_CONSTANT, data);
                    command.Parameters.AddWithValue(PublisherServiceQueries.TOPICID_CONSTANT, topicId);
                    command.Parameters.AddWithValue(PublisherServiceQueries.CLIENTID_CONSTANT, clientId);
                    connection.Open();
                    command.ExecuteReader();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        private Message fetchLatestMessage()
        {
            string queryString = PublisherServiceQueries.FETCH_LATEST_MESSAGE_QUERY;
            Message objMessage = new Message();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null)
                            {
                                objMessage.MessageId = Convert.ToInt32(reader[0]);
                                objMessage.Content = reader[1].ToString();
                                objMessage.TopicId = Convert.ToInt32(reader[2]);
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return objMessage;
        }
    }
}
