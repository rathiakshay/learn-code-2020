﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.DBQueries
{
    public class SubscriberServiceQueries
    {
        public const string GET_MESSAGE_BY_TOPICID_QUERY = "SELECT MessageContent,MessageId,TopicId FROM Messages where TopicId=@topicId and MessageState = 1;";
        public const string GET_MESSAGE_ALREADY_PRESENT_BY_CLIENTID_QUERY = "SELECT MessageID FROM SubscriberMessageMap where ClientId=@clientId;";
        public const string UPDATE_SUBSCRIBER_MESSAGE_MAP_TABLE_QUERY = "insert into SubscriberMessageMap (MessageID,ClientId) values(@messageId,@clientId);";
        public const string TOPICID_CONSTANT = "@topicId";
        public const string CLIENTID_CONSTANT = "@clientId";
        public const string MESSAGEID_CONSTANT = "@messageId";
    }
}
