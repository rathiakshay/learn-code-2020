﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.DBQueries
{
    public class PublisherServiceQueries
    {
        public const string CLIENTID_CONSTANT = "@clientID";
        public const string TOPICID_CONSTANT = "@topicID";
        public const string MessageID_CONSTANT = "@messageID";
        public const string DATA_CONSTANT = "@data";
        public const string MAP_PUBLISHER_TOPIC_QUERY = "insert into PublisherMessageMapping (ClientId,MessageId) values(@clientId ,@messageID);";
        public const string SAVE_DATA_INTO_MESSAGE_TABLE_QUERY = "insert into Messages (MessageContent,TopicId,ClientId) values(@data,@topicId,@clientId);";
        public const string SEARCH_CLIENT_BY_CLIENTNAME_QUERY = "SELECT UserName FROM  [dbo].[RootTable] where UserName = @clientName;";
        public const string FETCH_LATEST_MESSAGE_QUERY = "SELECT TOP 1 * FROM Messages  ORDER BY MessageID DESC;";
    }
}
