﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.DBQueries
{
    public class CommonServiceQueries
    {
        public const string SEARCH_CLIENT_BY_CLIENTNAME_QUERY = "SELECT UserName FROM  [dbo].[RootTable] where UserName = @clientName;";
        public const string IS_ROOT_TABLE_EXIST_QUERY = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like @tableName;"; 
        public const string GET_CLIENT_ID_BY_CLIENTNAME_QUERY = "SELECT UserId FROM RootTable where UserName = @clientName;";
        public const string ADD_CLIENT_QUERY = "insert into [dbo].[RootTable] values(@clientName);";
        public const string CREATE_NEW_TOPIC = "insert into topic values(@topicName);";
        public const string GET_TOPIC_QUERY = "SELECT TopicName,TopicId FROM Topic;";
        public const string UPDATE_MESSAGE_QUERY = "update Messages set MessageState = 0 WHERE TimeStampValue< (GETDATE() - 1);";
        public const string TABLENAME_CONSTANT = "@tableName";
        public const string CLIENTNAME_CONSTANT = "@clientName";
        public const string TOPICNAME_CONSTANT = "@topicName";

    }
}
