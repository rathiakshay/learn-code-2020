﻿using IMQModel;
using ServerService.DBQueries;
using ServerService.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService
{
    public class CommonService : ICommonService
    {
        string connectionString;
        private const string ROOT_TABLE_NAME = "RootTable";
        public CommonService()
        {
            connectionString = Config.connectionString;
        }
        public bool isClientExist(string clientName)
        {
            try
            {
                if (IsRootTableExist())
                {
                    string queryString = CommonServiceQueries.SEARCH_CLIENT_BY_CLIENTNAME_QUERY;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        SqlCommand command = new SqlCommand(queryString, connection);
                        command.Parameters.AddWithValue(CommonServiceQueries.CLIENTNAME_CONSTANT, clientName);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                if (reader != null && reader[0] != null)
                                {
                                    return true;
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return false;
        }
        public bool IsRootTableExist()
        {
            string queryString = CommonServiceQueries.IS_ROOT_TABLE_EXIST_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(CommonServiceQueries.TABLENAME_CONSTANT, ROOT_TABLE_NAME);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null && reader[0] != null)
                            {
                                return true;
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return false;
        }

        public int getClientIdByClientName(string clientName)
        {
            string queryString = CommonServiceQueries.GET_CLIENT_ID_BY_CLIENTNAME_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(CommonServiceQueries.CLIENTNAME_CONSTANT, clientName);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null)
                            {
                                int id = (int)reader[0];
                                return id;
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return -9999;
        }
        public void addClient(string clientName)
        {
            string queryString = CommonServiceQueries.ADD_CLIENT_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(CommonServiceQueries.CLIENTNAME_CONSTANT, clientName);
                    connection.Open();
                    command.ExecuteReader();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public List<Topic> getTopics()
        {
            string queryString = CommonServiceQueries.GET_TOPIC_QUERY;
            List<Topic> topics = new List<Topic>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader != null)
                            {
                                Topic topic = new Topic();
                                topic.TopicName = reader[0].ToString();
                                topic.TopicId = Convert.ToInt32(reader[1]);
                                topics.Add(topic);
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
            return topics;
        }

        public void updateMessageTable()
        {
            string queryString = CommonServiceQueries.UPDATE_MESSAGE_QUERY;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    command.ExecuteReader();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public string getClientNameFromClientIp(string clientIp)
        {
            try
            {
                string[] ipAddressParts = clientIp.Split(':');
                string clientName = "Client" + ipAddressParts[ipAddressParts.Length - 1];
                return clientName;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public void createNewTopic(string topic)
        {
            string queryString = CommonServiceQueries.CREATE_NEW_TOPIC;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue(CommonServiceQueries.TOPICNAME_CONSTANT, topic);
                    connection.Open();
                    command.ExecuteReader();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
