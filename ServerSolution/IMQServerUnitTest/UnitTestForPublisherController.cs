﻿using IMQModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerService;
using ServerSideController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest
{
    [TestClass]
    public class UnitTestForPublisherController
    {
        [TestMethod]
        public void TestMethodSetData()
        {            PublisherController objPublisherController = new PublisherController(new PublisherService());            string content = "From Test";            string clientIp = "27.0.0.1:12345";            int topicId = 5;            Message objMessage = objPublisherController.setData(content, clientIp,topicId);
            Assert.IsNotNull(objMessage);
            Assert.IsInstanceOfType(objMessage, typeof(Message));
            Assert.AreEqual(objMessage.Content, content);
            Assert.AreEqual(objMessage.TopicId, topicId);
        }
    }
}
