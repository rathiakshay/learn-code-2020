﻿using IMQModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerService;
using ServerSideController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest
{
    [TestClass]
    public class UnitTestForCommonController
    {
        [TestMethod]
        public void TestMethodGetTopics()
        {
            CommonController objCommonController = new CommonController(new CommonService());

            var actualResult = objCommonController.getTopics();
            var expectedResult = 0;

            Assert.AreNotEqual(expectedResult, actualResult.Count);
            Assert.IsInstanceOfType(actualResult, typeof(List<Topic>));
        }
        [TestMethod]
        public void TestMethodIsTopicExist()
        {
            CommonController objCommonController = new CommonController(new CommonService());

            var result = objCommonController.isTopicExist(1);

            Assert.IsTrue(result);
            Assert.IsInstanceOfType(result, typeof(bool));
        }
        [TestMethod]
        public void TestMethodGetClientNameFromClientIp()
        {
            CommonController objCommonController = new CommonController(new CommonService());

            string clientIp = "127.0.0.1:12345";
            var actualResult = objCommonController.getClientNameFromClientIp(clientIp);
            var expectedResult = "Client12345";

            Assert.AreEqual(actualResult, expectedResult);
            Assert.IsInstanceOfType(objCommonController.getClientIdByClientName("Client12345"), typeof(int));
        }
        [TestMethod]
        public void TestMethodGetClientIdByClientName()
        {
            CommonController objCommonController = new CommonController(new CommonService());

            string clientName = "Client12345";
            var result = objCommonController.getClientIdByClientName(clientName);
          
            Assert.IsInstanceOfType(result, typeof(int));
        }
    }
}
