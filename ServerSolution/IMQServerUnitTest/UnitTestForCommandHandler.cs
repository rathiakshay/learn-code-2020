﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerSolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest.UnitTestForIMQServer
{
    [TestClass]
    public class UnitTestForCommandHandler
    {
        [TestMethod]
        public void TestForEmptyCommand()
        {
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();
            CommandHandler objCommandHandler = new CommandHandler(objIMQueueHandler);
            try
            {
                objCommandHandler.handlerCommand("");
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is EmptyCommandInputException);
            }
        }

        [TestMethod]
        public void TestForInvalidCommand()
        {
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();
            CommandHandler objCommandHandler = new CommandHandler(objIMQueueHandler);
            try
            {
                objCommandHandler.handlerCommand("iqm");
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is InvalidCommandException);
            }
        }
        [TestMethod]
        public void TestForTopicDoesNotExits()
        {
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();
            CommandHandler objCommandHandler = new CommandHandler(objIMQueueHandler);
            try
            {
                objCommandHandler.displayQueueByTopicId("-9999");
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is TopicDoesNotExistException);
            }
        }
        [TestMethod]
        public void TestForCreateNewTopic()
        {
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();
            CommandHandler objCommandHandler = new CommandHandler(objIMQueueHandler);
            try
            {
                objCommandHandler.createNewTopic("cpp");
            }
            catch (Exception objException)
            {
                Assert.IsTrue(objException is TopicAlreadyPresentException);
            }
        }
    }
}
