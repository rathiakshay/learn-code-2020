﻿using IMQModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerService;
using ServerSideController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest
{
    [TestClass]
    public class UnitTestForSubscriberController
    {
        [TestMethod]
        public void TestMethodGetMessageByTopicId()
        {
            SubscriberController objSubscriberController = new SubscriberController(new SubscriberService(), new CommonService());
            List<Message> messages = objSubscriberController.getMessageByTopicId(1);

            Assert.IsNotNull(messages);
            Assert.IsTrue(messages.Count > 0);
        }
        [TestMethod]
        public void TestMethodGetMessagesAlreadyPresentForClient()
        {
            SubscriberController objSubscriberController = new SubscriberController(new SubscriberService(), new CommonService());
            List<int> seenMessagesId = objSubscriberController.getMessagesAlreadyPresentForClient("127.0.0.1:99999");
            Assert.IsTrue(seenMessagesId.Count == 0);
            Assert.IsInstanceOfType(objSubscriberController.getMessagesAlreadyPresentForClient("127.0.0.1:12345"), typeof(List<int>));

        }
    }
}
