﻿using IMQModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerSolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServerUnitTest
{
    [TestClass]
    public class UnitTestForIMQHandler
    {
        [TestMethod]
        public void TestMethodQueueDataByTopicId()
        {
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();

            objIMQueueHandler.setupIMQueues();
            List<Message> messages = objIMQueueHandler.getQueueDataByTopicId(1);

            Assert.IsNotNull(messages);
        }
    }
}
