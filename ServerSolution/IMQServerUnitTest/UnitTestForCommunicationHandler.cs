﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServerSolution;
using ProtocolLib;
using IMQModel;
using System.Collections.Generic;
using ServerSideController;
using ServerService;
using Moq;

namespace IMQServerUnitTest
{
    [TestClass]
    public class UnitTestForCommunicationHandler
    {
        [TestMethod]
        public void TestForCreateResponse()
        {
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(ConfigIMQServer.sourceURI);
            IMQueueHandler objIMQueueHandler = new IMQueueHandler();

            Response response = objCommunicationHandler.createResponse(ResponseType.SUCCESSFUL, ConfigIMQServer.sourceURI);

            Assert.IsNotNull(response);
            Assert.AreEqual(ResponseType.SUCCESSFUL, response.Status);
            Assert.AreEqual(ConfigIMQServer.sourceURI, response.SourceURI);
        }
    }
}
