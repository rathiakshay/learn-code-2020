﻿using IMQModel;
using ServerService;
using ServerSideController.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideController
{
    public class CommonController : ICommonController
    {
        private CommonService _objCommonService;
        public CommonController(CommonService objCommonService)
        {
            _objCommonService = objCommonService;
        }
        public string getClientNameFromClientIp(string clientIp)
        {
            try
            {
                return _objCommonService.getClientNameFromClientIp(clientIp);
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public int getClientIdByClientName(string clientName)
        {
            try
            {
                if (!_objCommonService.isClientExist(clientName))
                {
                    _objCommonService.addClient(clientName);
                }
                return _objCommonService.getClientIdByClientName(clientName);
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public List<Topic> getTopics()
        {
            try
            {
                return _objCommonService.getTopics();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public bool isTopicExist(int topicId)
        {
            List<Topic> topics = _objCommonService.getTopics();
            if (topics != null)
            {
                foreach (var topic in topics)
                {
                    if (topicId == topic.TopicId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool isTopicExistByTopicName(string topicName)
        {
            List<Topic> topics = getTopics();
            foreach (var topic in topics)
            {
                if (topic.TopicName.ToLower() == topicName.ToLower())
                {
                    return true;
                }
            }
            return false;
        }
        public void updateMessageTable()
        {
            try
            {
                _objCommonService.updateMessageTable();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public void createNewTopic(string topic)
        {
            try
            {
                _objCommonService.createNewTopic(topic);
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
