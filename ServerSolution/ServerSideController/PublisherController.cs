﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerService;
using IMQModel;

namespace ServerSideController
{
    public class PublisherController: IPublisherController
    {
        private PublisherService _objPublisherService;
        public PublisherController(PublisherService objPublisherService)
        {
            _objPublisherService = objPublisherService;
        }
        public Message setData(string message, string clientIp, int topicID)
        {
            try
            {
                return _objPublisherService.setData(message, clientIp, topicID);
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
