﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideController.Interface
{
    interface ICommonController
    {
        string getClientNameFromClientIp(string clientIp);
        int getClientIdByClientName(string clientName);
        List<Topic> getTopics();
        bool isTopicExist(int topicId);
        bool isTopicExistByTopicName(string topicName);
        void updateMessageTable();
        void createNewTopic(string topic);
    }
}
