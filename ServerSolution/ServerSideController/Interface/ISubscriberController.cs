﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideController.Interface
{
    interface ISubscriberController
    {
        List<Message> getMessageByTopicId(int topicId);
        List<int> getMessagesAlreadyPresentForClient(string clientIp);
        void updateSubscriberMessageMapTable(string clientIp, List<Message> messages);
    }
}
