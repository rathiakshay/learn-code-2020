﻿using IMQModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideController
{
    interface IPublisherController
    {
        Message setData(string message, string clientIp, int topicID);
    }
}
