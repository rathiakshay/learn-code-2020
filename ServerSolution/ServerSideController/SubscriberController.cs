﻿using IMQModel;
using ServerService;
using ServerSideController.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideController
{
    public class SubscriberController : ISubscriberController
    {
        private SubscriberService _objSubscriberService;
        private CommonService _objCommonService;
        public SubscriberController(SubscriberService objSubscriberService, CommonService objCommonService)
        {
            _objSubscriberService = new SubscriberService();
            _objCommonService = new CommonService();
        }

        public List<Message> getMessageByTopicId(int topicId)
        {
            try
            {
                List<Message> messages = _objSubscriberService.getMessageByTopicId(topicId);
                return messages;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public List<int> getMessagesAlreadyPresentForClient(string clientIp)
        {
            try
            {
                string clientName = _objCommonService.getClientNameFromClientIp(clientIp);
                int clientId = _objCommonService.getClientIdByClientName(clientName);
                List<int> messages = _objSubscriberService.getMessagesAlreadyPresentForClient(clientId);
                return messages;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public void updateSubscriberMessageMapTable(string clientIp, List<Message> messages)
        {
            try
            {
                string clientName = _objCommonService.getClientNameFromClientIp(clientIp);
                int clientId = _objCommonService.getClientIdByClientName(clientName);
                List<int> messageIdList = new List<int>();
                if (messages != null)
                {
                    foreach (var message in messages)
                    {
                        messageIdList.Add(message.MessageId);
                    }
                }
                _objSubscriberService.updateSubscriberMessageMapTable(clientId, messageIdList);
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
