﻿using IMQModel;
using ServerService;
using ServerSideController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServerSolution
{
    public class IMQueueHandler
    {
        List<IMQueue> objQueue;
        SubscriberController objSubscriberController;
        CommonController objCommonController;
        private System.Timers.Timer objTimer;
        public IMQueueHandler()
        {
            objSubscriberController = new SubscriberController(new SubscriberService(),new CommonService());
            objCommonController = new CommonController(new CommonService());
        }

        public void setupIMQueues()
        {
            List<Topic> topics = objCommonController.getTopics();
            objQueue = new List<IMQueue>();
            objCommonController.updateMessageTable();
            foreach (var topic in topics)
            {
                objQueue.Add(new IMQueue(topic.TopicId, topic.TopicName));
                List<Message> newMessages = objSubscriberController.getMessageByTopicId(topic.TopicId);
                foreach (var queue in objQueue)
                {
                    if (queue.QueueId == topic.TopicId)
                    {
                        foreach (var message in newMessages)
                        {
                            queue.Messages.Add(message);
                        }
                    }
                }
            }
        }
        public void scheduleTimerForUpdatingQueue()
        {
            objTimer = new System.Timers.Timer(3600000);
            objTimer.Elapsed += updateQueue;
            objTimer.AutoReset = true;
            objTimer.Enabled = true;
        }
        private void updateQueue(Object source, ElapsedEventArgs e)
        {
            setupIMQueues();
        }
        public List<Message> getQueueDataByTopicId(int toicId)
        {
            foreach (var queue in objQueue)
            {
                if (queue.QueueId == toicId)
                {
                    return queue.Messages;
                }
            }
            return new List<Message>();
        }

        public void addMessageToQueue(Message message, int topicId)
        {
            foreach (var queue in objQueue)
            {
                if (queue.QueueId == topicId)
                {
                    queue.Messages.Add(message);
                }
            }
        }
        public List<Message> filterUnseenMessages(List<int> seenMessageIds, int topicId)
        {
            List<Message> messages = new List<Message>();
            foreach (var queue in objQueue)
            {
                if (queue.QueueId == topicId)
                {
                    foreach (var item in queue.Messages)
                    {
                        bool isPresent = false;
                        if (seenMessageIds != null || seenMessageIds.Count > 1)
                        {
                            foreach (var messageId in seenMessageIds)
                            {
                                if (messageId == item.MessageId)
                                {
                                    isPresent = true;
                                }
                            }
                        }
                        if (!isPresent)
                        {
                            messages.Add(item);
                        }
                    }
                }
            }
            return messages;
        }
    }
}
