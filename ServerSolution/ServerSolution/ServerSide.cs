﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerSolution
{
    class ServerSide
    {
        static void Main(string[] args)
        {
            ServerService objServerService = new ServerService();
            if (objServerService.startServer())
            {
                Console.WriteLine("Server has started");
            }
        }
    }
}

