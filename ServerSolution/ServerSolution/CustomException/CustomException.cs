﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSolution
{
    public class InvalidCommandException : Exception
    {
        public InvalidCommandException()
            : base("Command is Invalid, Please enter it again or for help type \"imq help\"")
        {
        }
    }
    public class EmptyCommandInputException : Exception
    {
        public EmptyCommandInputException()
            : base("Command line input empty, Please enter it again or for help type \"imq help\"")
        {
        }
    }
    public class TopicAlreadyPresentException : Exception
    {
        public TopicAlreadyPresentException()
            : base("Please type another topic name because topic is already exits")
        {
        }
    }
    public class InvalidTopicConnectionException : Exception
    {
        public InvalidTopicConnectionException()
            : base("Provided topic in command is not connected, Please enter it again or to connect new topic type \"imq connect \"topic name\"\"")
        {
        }
    }
    public class EmptyQueueException : Exception
    {
        public EmptyQueueException()
            : base("No message present in queue")
        {
        }
    }
    public class TopicDoesNotExistException : Exception
    {
        public TopicDoesNotExistException()
            : base("Provided topicId does not exist")
        {
        }
    }
}
