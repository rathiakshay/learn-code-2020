﻿using Newtonsoft.Json;
using ProtocolLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServerSideController;
using System.Collections;
using IMQModel;
using ServerService;

namespace ServerSolution
{
    public class ServerService
    {
        static TcpListener listener;
        string sourceURI = ConfigIMQServer.sourceURI;
        IMQueueHandler objIMQueueHandler;
        CommonController objCommonController;
        CommandHandler objCommandHandler;
        Dictionary<int, int> topicClientConnection;
        public ServerService()
        {
            listener = new TcpListener(System.Net.IPAddress.Parse(ConfigIMQServer.ipAddress), ConfigIMQServer.portNumber);
            objIMQueueHandler = new IMQueueHandler();
            objCommonController = new CommonController(new CommonService());
            objCommandHandler = new CommandHandler(objIMQueueHandler);
            topicClientConnection = new Dictionary<int, int>();
        }
        public bool startServer()
        {
            try
            {
                listener.Start();
                Console.WriteLine("************This is Server program************" + listener.LocalEndpoint.ToString());
                Console.WriteLine("How many clients are going to connect to this server?:");
                int numberOfClientsYouNeedToConnect = int.Parse(Console.ReadLine());

                for (int i = 0; i < numberOfClientsYouNeedToConnect; i++)
                {
                    Thread newThread = new Thread(new ThreadStart(Listeners));
                    newThread.Start();
                }

                Thread inputThread = new Thread(new ThreadStart(inputHandler));
                inputThread.Start();
                objIMQueueHandler.setupIMQueues();
                objIMQueueHandler.scheduleTimerForUpdatingQueue();
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        private void inputHandler()
        {
            string inputData = IMQServerConstant.TYPE_MESSAGE_CONSTANT;
            Console.WriteLine(inputData);

            while (inputData.ToLower() != IMQServerConstant.EXIT_CONSTANT)
            {
                try
                {
                    inputData = Console.ReadLine();
                    objCommandHandler.handlerCommand(inputData);
                }
                catch (Exception objException)
                {
                    Console.WriteLine(objException.Message);
                }
            }
        }
        private void Listeners()
        {
            Socket socketForClient = listener.AcceptSocket();
            Response response = new Response();
            NetworkStream networkStream = new NetworkStream(socketForClient);
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(sourceURI);

            try
            {
                if (socketForClient.Connected)
                {
                    Console.WriteLine(IMQServerConstant.CLIENT_CONSTANT + ":" + socketForClient.RemoteEndPoint + IMQServerConstant.CONNECTION_MESSAGE_CONSTANT);

                    while (true && socketForClient.Connected)
                    {
                        try
                        {
                            do
                            {
                                Request request = objCommunicationHandler.getRequestFromClient(networkStream);
                                string clientIp = socketForClient.RemoteEndPoint.ToString();
                                if (request != null)
                                {
                                    if (request.RequestType.ToLower() == RequestType.EXIT)
                                    {
                                        response = objCommunicationHandler.createResponse(ResponseType.SUCCESSFUL, clientIp);
                                        objCommunicationHandler.sendResponseToClient(response, networkStream);
                                        objCommunicationHandler.closeConnection(socketForClient, networkStream);
                                        break;
                                    }
                                    else
                                    {
                                        response = requestHandler(request, socketForClient.RemoteEndPoint.ToString());
                                        objCommunicationHandler.sendResponseToClient(response, networkStream);
                                    }
                                }
                            } while (networkStream.DataAvailable);
                        }
                        catch (Exception objException)
                        {
                            Console.WriteLine(objException.Message);
                            continue;
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.Message);
                Console.WriteLine(socketForClient.RemoteEndPoint.ToString() + IMQServerConstant.DISCONNECTION_MESSAGE_CONSTANT);
                objCommunicationHandler.closeConnection(socketForClient, networkStream);
            }
        }
        public Response requestHandler(Request request, string clientIp)
        {
            Response response = new Response();

            if (request != null)
            {
                Console.WriteLine("\n" + clientIp + ">>\n" + IMQServerConstant.REQUEST_CONSTANT + ":" + request.RequestType);
                switch (request.RequestType.ToLower())
                {
                    case RequestType.CONNECT_TO_TOPIC:
                        response = connectToTopic(request);
                        break;
                    case RequestType.GET_TOPICS:
                        response = getAvailableTopics(clientIp);
                        break;
                    case RequestType.PUBLISH:
                        response = publishMessageToTopic(request.Data[0], clientIp, request.TopicId);
                        break;
                    case RequestType.GET_TOPIC_DATA:
                        response = fetchTopicContent(request.TopicId, clientIp);
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine(clientIp + ">>\n" + IMQServerConstant.RESPONSE_CONSTANT + ": " + response.Status + "\n");
            return response;
        }
        private Response connectToTopic(Request request)
        {
            CommonController objCommonController = new CommonController(new CommonService());
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(sourceURI);
            Response response = new Response();

            string clientIp = objCommonController.getClientNameFromClientIp(request.SourceURI);
            int clientId = objCommonController.getClientIdByClientName(clientIp);
            if (!topicClientConnection.ContainsKey(clientId))
            {
                topicClientConnection.Add(clientId, request.TopicId);
            }
            else
            {
                topicClientConnection[clientId] = request.TopicId;
            }
            response = objCommunicationHandler.createResponse(ResponseType.CONNECTED_WITH_TOPIC_SUCCESSFULLY, request.SourceURI);
            response.TopicId = request.TopicId;

            return response;
        }
        private Response getAvailableTopics(string clientIp)
        {
            CommonController objCommonController = new CommonController(new CommonService());
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(sourceURI);
            Response response = new Response();

            List<Topic> topics = objCommonController.getTopics();
            List<string> topicsName = new List<string>();
            if (topics != null)
            {
                foreach (var topic in topics)
                {
                    topicsName.Add(topic.TopicName);
                }
                response = objCommunicationHandler.createResponse(ResponseType.SUCCESSFUL, clientIp, topicsName);
            }
            else
            {
                response = objCommunicationHandler.createResponse(ResponseType.NO_TOPIC_AVALIABLE, clientIp, topicsName);
            }
            return response;
        }
        private Response publishMessageToTopic(string message, string clientIp, int topicId)
        {
            PublisherController objPublisherController = new PublisherController(new PublisherService());
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(sourceURI);

            Message objMessage = objPublisherController.setData(message, clientIp, topicId);
            objIMQueueHandler.addMessageToQueue(objMessage, topicId);
            Response response = objCommunicationHandler.createResponse(ResponseType.PUBLISHED, clientIp);
            return response;
        }
        private Response fetchTopicContent(int topicId, string clientIp)
        {
            SubscriberController objSubscriberController = new SubscriberController(new SubscriberService(),new CommonService());
            CommunicationHandler objCommunicationHandler = new CommunicationHandler(sourceURI);
            Response response;
            List<string> topicMessages = new List<string>();

            string[] ipAddressParts = clientIp.Split(':');
            string clientName = IMQServerConstant.CLIENT_CONSTANT + ipAddressParts[ipAddressParts.Length - 1];
            List<int> seenMessageIds = objSubscriberController.getMessagesAlreadyPresentForClient(clientIp);
            List<Message> messages = objIMQueueHandler.filterUnseenMessages(seenMessageIds, topicId);

            if (messages == null || messages.Count == 0)
            {
                response = objCommunicationHandler.createResponse(ResponseType.NO_NEW_MESSAGE, clientIp, topicMessages);
            }
            else
            {
                objSubscriberController.updateSubscriberMessageMapTable(clientIp, messages);
                foreach (var message in messages)
                {
                    topicMessages.Add(message.Content);
                }
                response = objCommunicationHandler.createResponse(ResponseType.SUCCESSFUL, clientIp, topicMessages);
            }
            return response;
        }
    }
}
