﻿using Newtonsoft.Json;
using ProtocolLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ServerSolution
{
    public class CommunicationHandler
    {
        string sourceURI;
        public CommunicationHandler(string sourceURI)
        {
            this.sourceURI = sourceURI;
        }

        public Response createResponse(string responseStatus, string clientIp, [Optional]List<string> data)
        {
            Response response = new Response();
            if (data == null)
            {
                data = new List<string>();
            }
            response.Data = data;
            response.Status = responseStatus;
            response.SourceURI = sourceURI;
            response.DestinationURI = clientIp;
            return response;
        }
        public void sendResponseToClient(Response response, NetworkStream networkStream)
        {
            StreamWriter streamWriter = new StreamWriter(networkStream);
            streamWriter.WriteLine(JsonConvert.SerializeObject(response));
            streamWriter.Flush();
        }

        public Request getRequestFromClient(NetworkStream networkStream)
        {
            StreamReader streamReader = new StreamReader(networkStream);
            byte[] readBuffer = new byte[2048];
            int numberOfBytesRead = networkStream.Read(readBuffer, 0, readBuffer.Length);
            Request request = JsonConvert.DeserializeObject<Request>(Encoding.ASCII.GetString(readBuffer, 0, numberOfBytesRead).ToString());
            return request;
        }
        public void closeConnection(Socket socketForClient, NetworkStream networkStream)
        {
            Console.WriteLine(socketForClient.RemoteEndPoint + " has been disconnected");
            networkStream.Close();
            socketForClient.Close();
        }
    }
}
