﻿using IMQModel;
using ServerService;
using ServerSideController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSolution
{
    public class CommandHandler
    {
        CommonController objCommonController;
        IMQueueHandler objIMQueueHandler;
        public CommandHandler(IMQueueHandler objIMQueueHandler)
        {
            this.objCommonController = new CommonController(new CommonService());
            this.objIMQueueHandler = objIMQueueHandler;
        }
        public void handlerCommand(string commandString)
        {
            try
            {
                if (commandString == null || commandString == "")
                {
                    throw new EmptyCommandInputException();
                }
                string[] commandStringArray = commandString.Split(null);

                if (commandStringArray != null && isCommandStringValid(commandStringArray))
                {
                    switch (commandStringArray[1].ToLower())
                    {
                        case CommandConstant.CREATE:
                            createNewTopic(commandStringArray[2]);
                            break;
                        case CommandConstant.SHOW:
                            displayQueueByTopicId(commandStringArray[2]);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    throw new InvalidCommandException();
                }
            }
            catch (Exception objexception)
            {
                throw objexception;
            }
        }
        public bool isCommandStringValid(string[] commandStringArray)
        {
            if (commandStringArray != null && (commandStringArray[0].ToLower() == CommandConstant.PREFIX))
            {
                if (commandStringArray[1] != null && (commandStringArray[1].ToLower() == CommandConstant.CREATE ||
                   commandStringArray[1].ToLower() == CommandConstant.SHOW
                   ))
                {
                    switch (commandStringArray[1].ToLower())
                    {
                        case CommandConstant.CREATE:
                            if (commandStringArray[2] != null)
                            {
                                return true;
                            }
                            break;
                        case CommandConstant.SHOW:
                            if (commandStringArray[2] != null)
                            {
                                return true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return false;
        }

        public void createNewTopic(string topicName)
        {
            try
            {
                if (objCommonController.isTopicExistByTopicName(topicName))
                {
                    throw new TopicAlreadyPresentException();
                }
                else
                {
                    objCommonController.createNewTopic(topicName);
                    Console.WriteLine(topicName + IMQServerConstant.TOPIC_CREATED_SUCCESSFULLY);
                    objIMQueueHandler.setupIMQueues();
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        public void displayQueueByTopicId(string topicId)
        {
            try
            {
                if (!string.IsNullOrEmpty(topicId))
                {
                    if (objCommonController.isTopicExist(Int32.Parse(topicId)))
                    {
                        List<Message> messages = objIMQueueHandler.getQueueDataByTopicId(Int32.Parse(topicId));
                        if (messages != null && messages.Count > 0)
                        {
                            foreach (var item in messages)
                            {
                                Console.WriteLine(item.MessageId.ToString() + ":" + item.Content);
                            }
                        }
                        else
                        {
                            throw new EmptyQueueException();
                        }
                    }
                    else
                    {
                        throw new TopicDoesNotExistException();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
    }
}
