﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSolution
{
    public class CommandConstant
    {
        public const string PREFIX = "imq";
        public const string HELP = "help";
        public const string SHOW = "show";
        public const string CREATE = "create";
    }
}
