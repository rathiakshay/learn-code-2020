﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSolution
{
    public class IMQServerConstant
    {
        public const string CLIENT_CONSTANT = "Client";
        public const string DISCONNECTION_MESSAGE_CONSTANT = " has be disconnected";
        public const string CONNECTION_MESSAGE_CONSTANT = " now connected to server.";
        public const string TYPE_MESSAGE_CONSTANT = "type ->imq show \"topicId\" to display queue data and ->imq create \"topicName\" to create new topic ";
        public const string EXIT_CONSTANT = "exit";
        public const string REQUEST_CONSTANT = "request";
        public const string RESPONSE_CONSTANT = "response";
        public const string TOPIC_DOES_NOT_EXIST_MESSAGE_CONSTANT = "Topic does not exist";
        public const string NO_MESSAGE_IN_QUEUE_MESSAGE_CONSTANT = "No message in queue";
        public const string TOPIC_CREATED_SUCCESSFULLY = "Topic created";
    }
}
