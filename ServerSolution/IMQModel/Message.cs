﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQModel
{
    public class Message
    {
        public int MessageId;
        public string Content;
        public int TopicId;
    }
}
