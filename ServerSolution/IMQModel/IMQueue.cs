﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQModel
{
    public class IMQueue
    {
        public IMQueue(int topicId,string topicName)
        {
            this.QueueId = topicId;
            this.TopicName = topicName;
            this.Messages = new List<Message>();
        }
        public int QueueId;
        public string TopicName;
        public List<Message> Messages;
    }
}
