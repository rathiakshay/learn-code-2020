﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolLib
{
    public class Request:IMQProtocol
    {
        public string RequestType { get; set; }
        public string Role { get; set; }
    }
}
