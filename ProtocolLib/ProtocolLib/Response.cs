﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolLib
{
    public class Response:IMQProtocol
    {
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
