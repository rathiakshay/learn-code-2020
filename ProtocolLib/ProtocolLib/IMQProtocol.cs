﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolLib
{
    public class IMQProtocol
    {
        public IMQProtocol()
        {
            this.Version = "1.0";
            this.DataFormatType = "JSON";
        }
        public string Version { get; set; }
        public string SourceURI { get; set; }
        public string DestinationURI { get; set; }
        public string DataFormatType { get; set; }
        public int TopicId { get; set; }
        public List<string> Data { get; set; }
    }
}
