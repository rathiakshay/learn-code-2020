﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolLib
{
    public class ResponseType
    {
        public const string SUCCESSFUL = "Successful";
        public const string UNSUCCESSFUL = "Unsuccessful";
        public const string PUBLISHED = "Published";
        public const string NO_TOPIC_AVALIABLE = "No Topic Available";
        public const string NO_NEW_MESSAGE = "No new message";
        public const string CONNECTED_WITH_TOPIC_SUCCESSFULLY = "CONNECTED WITH TOPIC SUCCESSFULLY";
    }
}
