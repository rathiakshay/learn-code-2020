﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolLib
{
    public class RequestType
    {
        public const string GET_TOPICS = "get_topics";
        public const string PUBLISH = "publish";
        public const string CONNECT_TO_TOPIC = "connect_to_topic";
        public const string GET_TOPIC_DATA = "get_topic_data";
        public const string EXIT = "exit";
    }
}
